<?php
/**
 * @file
 * Contains merge process for merging a data set.
 */

class PartyMergeProcessMergeDataSet extends EntityMergeProcessBase {

  /**
   * Overrides EntityMergeProcessBase::registerSubProcesses()
   */
  public function registerSubProcesses() {
    $data_set_name = $this->conf['data_set_name'];
    $info = party_get_data_set_info($data_set_name);
    $class = $info['class'];

    $controller1 = $this->entity1->getDataSetController($data_set_name);
    $controller2 = $this->entity2->getDataSetController($data_set_name);
    $processes = $class::registerMergeProcesses($controller1, $controller2);

    return $processes;
  }

  /**
   * Overrides EntityMergeProcessBase::run()
   */
  public function run(&$context = array()) {
    $data_set_name = $this->conf['data_set_name'];
    $info = party_get_data_set_info($data_set_name);
    $class = $info['class'];

    $controller1 = $this->entity1->getDataSetController($data_set_name);
    $controller2 = $this->entity2->getDataSetController($data_set_name);
    $class::runMerge($controller1, $controller2);

    parent::run($context);
  }

  /**
   * {@inheritdoc}
   */
  public function getProgressMessage(&$context = array()) {
    $info = party_get_data_set_info($this->conf['data_set_name']);
    return t('Merging @data_set', array('@data_set' => $info['label']));
  }

  /**
   * {@inheritdoc}
   */
  public function previewRows($prefix = 'data_set:') {
    $ds_name = $this->conf['data_set_name'];
    $prefix .= $this->conf['data_set_name'].':';

    $info = party_get_data_set_info($ds_name);
    $class = $info['class'];

    $ds_rows = $class::mergePreviewRows(
      $this->entity1->getDataSetController($ds_name),
      $this->entity2->getDataSetController($ds_name)
    );

    $rows = array();
    if (!empty($ds_rows)) {
      $rows[$prefix.':header'] = array(
        array(
          'colspan' => 3,
          'class' => array('entity-merge-ui-header'),
          'header' => TRUE,
          'data' => $info['label'],
        ),
      );

      foreach ($ds_rows as $name => $row) {
        $rows[$prefix.':'.$name] = $row;
      }
    }

    return $rows;
  }
}
