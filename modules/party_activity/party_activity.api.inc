<?php

/**
 * @file
 * Hook Documentation for Party Activity.
 */

/**
 * Make changes to an activity when it is created.
 *
 * Perform logic when a new activity object is created. Note that not all
 * created activities get saved. This hook is useful for setting default values
 * before the activity is edited in a form.
 *
 * @param PartyActivity $activity
 *   The activity object.
 */
function hook_party_activity_create($activity) {
  // No Example.
}
